// Include D3.js library
// You can download it or include it from a CDN
// Example: <script src="https://d3js.org/d3.v5.min.js"></script>


// Number of data points
const numPoints = 100;

// Generate time (x-axis) data
const timeData = Array.from({ length: numPoints }, (_, i) => i + 1);



// Create the first chart
const margin = { top: 20, right: 20, bottom: 40, left: 90 };
const width = 800 - margin.left - margin.right;
const height = 300 - margin.top - margin.bottom;

const svg1 = d3.select("#chart1")
  .append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Create scales
const xScale = d3.scaleLinear().domain([1, numPoints]).range([0, width]);
const yScale = d3.scaleLinear().domain([0, 100000]).range([height, 0]);

// Create the tooltip as an SVG group (g) containing rect and text elements
const tooltip = svg1.append("g")
    .attr("class", "tooltip")
    .style("opacity", 0);
    
// Add a white background rectangle
tooltip.append("rect")
    .attr("width", 250)  // Adjust the width as needed
    .attr("height", 30)  // Adjust the height as needed
    .attr("fill", "white")
    .attr("stroke", "black")
    .attr("stroke-width", 1);
    
// Add text inside the rectangle
tooltip.append("text")
    .attr("x", 5)   // Adjust the x-position of text inside the rectangle
    .attr("y", 20)  // Adjust the y-position of text inside the rectangle
    .style("fill", "black");
    
// Create the vertical line
const verticalLine = svg1.append("line")
    .attr("class", "vertical-line")
    .attr("y1", 0)
    .attr("y2", height)
    .style("opacity", 0); // Initially hidden

    
// Attach mouse event listeners to the SVG container
svg1
    .on("mouseover", function () {
        verticalLine
            .style("opacity", 1)
            .raise();
        tooltip.transition()
            .style("opacity", 1)
    })
    .on("mousemove", function (event) {
        const [x, y] = d3.pointer(event, this);
        
        // Get the y-values for both functions at the current x-value
        const xValue = Math.floor(xScale.invert(x));
        const yValue1 = yData[Math.round(xScale.invert(x))];
        const yValue2 = yData2[Math.round(xScale.invert(x))];

        tooltip
//             .attr("transform", `translate(${x},${0})`)  // move tooltip with mouse
            .attr("transform", `translate(${5},${5})`)
            .style("opacity", 1)
            .raise();
        // Update the content of the text element
        tooltip.select("text")
//             .text(`Age: ${xValue} Income: ${yValue1.toFixed(0)} Expenses: ${yValue2.toFixed(0)}`);
            .text(`Income: ${yValue1.toFixed(0)} Expenses: ${yValue2.toFixed(0)}`);
            
        verticalLine
            .attr("x1", x)
            .attr("x2", x)
            .raise();
    })
    .on("mouseout", function () {
        // Hide the vertical line on mouseout
//         verticalLine
//             .style("opacity", 0);
//         tooltip.transition()
//             .duration(500)
//             .style("opacity", 0);
    });
    

//////////////////////////
//// INCOME

// Generate y-axis data based on the specified conditions
const yData = timeData.map(year => {
  if (year <= 18) {
    return 0;
  } else if (year <= 25) {
    return 20000;
  } else if (year <= 55) {
    return 60000 + (year-25) * (40000/30);
  } else if (year <= 65) {
    return 100000;
  } else {
    // Linear decrease from 100k to 30k
    return 30000;
  }
});

// Create the line
const line = d3.line()
  .x((d, i) => xScale(timeData[i]))
  .y(d => yScale(d));
  
// Create the area below the line
const area = d3.area()
    .defined(line.defined())
    .x((d, i) => xScale(timeData[i]))
    .y0(height)
    .y1(d => yScale(d));

// Append the area to the chart
svg1.append("path")
    .data([yData])
    .attr("class", "area_income") // Add the class to the area
    .attr("d", area)
    
    
//////////////////////////
//// EXPENSES
    
// Create the second y-axis data based on your conditions
const yData2 = timeData.map(year => {
  if (year <= 18) {
    return 0;
  } else if (year <= 25) {
    return 15000;
  } else if (year <= 55) {
    return 20000 + (year-25) * (20000/30);
  } else if (year <= 65) {
    return 40000 + (year-55) * (-10000/10);
  } else {
    // Linear decrease from 100k to 30k
    return 25000;
  }
});

// Create the second line
const line2 = d3.line()
    .defined(d => d !== null)
    .x((d, i) => xScale(timeData[i]))
    .y(d => yScale(d));

// Create the second area below the line
const area2 = d3.area()
    .defined(line2.defined())
    .x((d, i) => xScale(timeData[i]))
    .y0(height)
    .y1(d => yScale(d));

// Append the second area to the chart
svg1.append("path")
    .data([yData2])
    .attr("class", "area_expenses") // Add the class to the second area
    .attr("d", area2)
    
    
//////////////////////////    
//// Axes and labels

// Add x-axis
svg1.append("g")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(xScale));

// Add y-axis
svg1.append("g")
  .call(d3.axisLeft(yScale));

// Optional: Add labels, title, etc. for better visualization
svg1.append("text")
  .attr("transform", "translate(" + (width / 2) + " ," + (height + margin.top + 10) + ")")
  .style("text-anchor", "middle")
  .text("Time (Years)");

svg1.append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 0 - margin.left)
  .attr("x", 0 - (height / 2))
  .attr("dy", "1em")
  .style("text-anchor", "middle")
  .text("Income and Expenses");

  
  
  
  
  
    
//////////////////////////
//// WEALTH
// Your code to generate the second stacked line chart goes here

// Create the second chart
const chart2 = d3.select('#chart2')
    .append('svg')
    .attr('width', '100%')
    .attr('height', '100%')
    .append('g');

    
    
// Add interactivity to charts, e.g., mouseover events, tooltips, etc.

// Your code for dynamic pop-ups based on mouse position goes here


    
// Your data manipulation logic goes here
// Update data1 and data2 based on user inputs and formulas
    
    
    
// Your code for interactivity (e.g., mouseover events, tooltips) goes here



function drawsvg(sorted) {
    // number of days per month, starting in March to February
    const months = [1,31,30,31,30,31,31,30,31,30,31,31,27];
    let y = 0;
    const monthsCumsum = months.map(d=>y+=d);
    const monthNames = ['Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez','Jan','Feb'];
    // get all svg elements in webpage
    var svgs = document.getElementsByTagName('svg');
    for(let i=0; i<svgs.length; i++){
        svg = svgs[i];
        // extract opening and closing dates from html element
        [openCoords,closeCoords] = getDates(svg.dataset,sorted);
        // get width and height of svg drawing area
        const   w = (svg.width.baseVal.value-2);
        const  dw = (svg.width.baseVal.value-2)/365;
        const   h = (svg.height.baseVal.value-1);
        const dho = -(h-1)/openCoords.length;
        const dhc = (h-1)/closeCoords.length;

        // create a new path 
        var element = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        var openPath = 'M'+(0)+' '+(h)  // start path in lower left corner, offcenter one right and one up to show whole line
        for (let step = 0; step < openCoords.length; step++) {
            openPath = [openPath,'H' +   openCoords[step]*dw].join(' ');
            openPath = [openPath,'v' + dho].join(' ');
        }        
        for (let step = 0; step < closeCoords.length; step++) {
            openPath = [openPath,'H' + closeCoords[step]*dw].join(' ');
            openPath = [openPath,'v' + dhc].join(' ');
        }
        var openPath = [openPath,'L'+(w)+' '+(h)].join(' ')  // end path in lower right corner, offcenter one right and one up to show whole line
        element.setAttribute('d', openPath);   // set path's data
        element.setAttribute('class', 'openPath');   // set path's class
        svg.appendChild(element);  // add path element to svg

        // add dark baseline
        var baseLine = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        var baseLineData = 'M'+(0)+' '+(h)  // start path in lower left corner
        var baseLineData = [baseLineData,'L'+(w)+' '+(h)].join(' ')  // end path in lower right corner
        baseLine.setAttribute('d', baseLineData);   // set path's data
        baseLine.setAttribute('class', 'baseLine');   // set path's class
        svg.appendChild(baseLine);  // add path element to svg

        // add month markers on hover
        for (let step = 0; step < monthsCumsum.length-1; step++) {
            var monthLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            monthLine.setAttribute('x1', monthsCumsum[step]*dw-0.5*dw);
            monthLine.setAttribute('x2', monthsCumsum[step]*dw-0.5*dw);
            monthLine.setAttribute('y1', h);
            monthLine.setAttribute('y2', 0.9*h);
            monthLine.setAttribute('class', 'monthMarker');
            monthLine.setAttribute('id', monthNames[step]);
            svg.appendChild(monthLine);  // add monthLine element to svg
            
            var newText = document.createElementNS('http://www.w3.org/2000/svg', 'text');
            newText.setAttribute("x",monthsCumsum[step]*dw+1.5*dw);
            newText.setAttribute("y",0.96*h-dw);
            newText.setAttribute('class', 'monthMarkerText');
            newText.textContent = monthNames[step];
            svg.appendChild(newText); 
        }

    // add line at current date
    var todayMonths = new Date().getMonth();
    if (todayMonths < 2) {
        todayMonths += 10;
    } else {
        todayMonths += -2;
    }
    const todayDays = new Date().getDate() + monthsCumsum[todayMonths] - 1;
    var todayLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    todayLine.setAttribute('x1', todayDays*dw);
    todayLine.setAttribute('x2', todayDays*dw);
    todayLine.setAttribute('y1', h);
    todayLine.setAttribute('y2', 0.5*h);
    todayLine.setAttribute('class', 'todayMarker');
    todayLine.setAttribute('id', 'todayMarker');
    svg.appendChild(todayLine);  // add todayLine element to svg
    }
}

function getDates(dataset,sorted) {
    // get date list from html dataset
    var openDates  = dataset.open.split(',');
    var closeDates = dataset.close.split(',');
    openCoords = [];
    closeCoords = [];
    // convert date items to consecutive days
    for (let i=0; i<openDates.length; i++) {
        openCoords.push(dateToDays(openDates[i]));
    }
    openCoords = openCoords.reverse();
    for (let i=0; i<closeDates.length; i++) {
        closeCoords.push(dateToDays(closeDates[i]));
    }
    // sort both array numerically ascending
    if (sorted) {
        openCoords  = openCoords.sort((a, b) => a-b);
        closeCoords  = closeCoords.sort((a, b) => a-b);
    }
    console.log(closeCoords);
    return [openCoords,closeCoords];  // return open and close dates in coordinates: x = [1,365], y=[0,1]
}


function dateToDays(dateString) {
    // number of days per month, starting in March to February
    const months = [31,30,31,30,31,31,30,31,30,31,31,28];
    // get month number from datestring and sum up all days of previous months
    var totalDays = months.slice(0,dateString.split('/')[1]-3).reduce((totalValue, currentValue) => totalValue + currentValue, 0);
    // add single days to get total number of days
    totalDays += parseInt(dateString.split('/')[2]);
    return totalDays
}

// redraw complete figure on window resize
window.onresize = function() {
    var svgs = document.getElementsByTagName('svg');
    for(let i=0; i<svgs.length; i++){
        svg = svgs[i];
        while (svg.firstChild) {
            svg.removeChild(svg.firstChild);
        }
    }
    if(document.getElementById('sortToggle').checked) {
        //Toggle is on
        drawsvg(sorted=true)
    } else {
        //Toggle is off
        drawsvg(sorted=false)
    }
};

// change plotting on toggle switch
document.getElementById('sortToggle').onchange = function(e){
    var svgs = document.getElementsByTagName('svg');
    for(let i=0; i<svgs.length; i++){
        svg = svgs[i];
        while (svg.firstChild) {
            svg.removeChild(svg.firstChild);
        }
    }
    if(e.target.checked) {
        //Toggle is on
        drawsvg(sorted=true)
    } else {
        //Toggle is off
        drawsvg(sorted=false)
    }
};

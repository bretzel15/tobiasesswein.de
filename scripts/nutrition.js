let selectedIngredients = [];

function addSearchedIngredient() {
  const searchInput = document.getElementById('ingredientSearch');
  const ingredient = searchInput.value.trim();
    
  if (ingredient !== '') {
    selectedIngredients.push(ingredient);
  
    const selectedList = document.getElementById('selectedIngredients');
    const li = document.createElement('li');
    li.textContent = ingredient;
    selectedList.appendChild(li);
  
    searchInput.value = ''; // Clear the search input after adding the ingredient
  }
}

function calculateNutrition() {
  fetch('ingredients.csv')
    .then(response => response.text())
    .then(data => {
      const ingredientNames = extractIngredientNames(data);
      allIngredients = ingredientNames; // Store all ingredient names for search
      console.log(selectedIngredients)
      calculateMealNutrition(selectedIngredients);
    })
    .catch(error => {
      console.error('Error fetching CSV file:', error);
    });
}

function extractIngredientNames(data) {
  const lines = data.trim().split('\n');
  const header = lines.shift().split(','); // Extract headers and discard
  const ingredientNames = lines.map(line => {
    const values = line.split(',');
    return values[0]; // Assuming ingredient names are in the first column
  });
  console.log(ingredientNames)
  return ingredientNames;
}

function parseCSV(data) {
  allIngredients = extractIngredientNames(data); // Store all ingredient names for search
  // Your existing code for parsing CSV (adjustments to handle header row)
}

function calculateMealNutrition(nutritionalValues) {
  const totalNutrition = { 'Calories': 0, 'Protein': 0, 'Fat': 0, 'Carbs': 0 };

  selectedIngredients.forEach(ingredient => {
    if (nutritionalValues.hasOwnProperty(ingredient)) {
      for (let nutrient in totalNutrition) {
        totalNutrition[nutrient] += nutritionalValues[ingredient][nutrient];
      }
    }
  });

  const mealNutrition = document.getElementById('mealNutrition');
  mealNutrition.innerHTML = '';
  for (let nutrient in totalNutrition) {
    const li = document.createElement('li');
    li.textContent = `${nutrient}: ${totalNutrition[nutrient]}g`;
    mealNutrition.appendChild(li);
  }
}

document.addEventListener('DOMContentLoaded', () => {
  fetch('ingredients.csv')
    .then(response => response.text())
    .then(data => {
      ingredientNames = extractIngredientNames(data);
      console.log(ingredientNames)
    })
    .catch(error => {
      console.error('Error fetching CSV file:', error);
    });
});
